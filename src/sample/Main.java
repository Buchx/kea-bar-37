package sample;

import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.awt.*;

public class Main extends Application {


    Stage window;
    Scene scene1, scene2;



    @Override
    public void start(Stage primaryStage) throws Exception{
        window = primaryStage;

        Parent root = null;
        root = FXMLLoader.load(getClass().getResource("Views/Main.fxml"));

        window.setTitle("KEA - LYGTEN 37 - BAR");
        scene1 = new Scene(root);


        try{
            scene1.getStylesheets().add(getClass().getResource("Resources/material-fx-v0_3.css").toExternalForm());
        }catch (Exception e){
            System.out.println(e.getMessage());
        }


        window.setScene(scene1);
        window.show();

    }



    public static void main(String[] args) {
        launch(args);
    }
}
