package sample.Controllers;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import sample.Logic.ProduktListe;
import sample.Objects.Produkt;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class productController implements Initializable {

    @FXML
    GridPane gPane;
    @FXML
    AnchorPane mainPane;
    private ProduktListe l = new ProduktListe();
    private ArrayList<Produkt> produkter = l.produktListe;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        System.out.println("hello");
        createProducts();
    }

    private void createProducts() {
        gPane.setVgap(10);
        gPane.setHgap(30);
        gPane.setPadding(new Insets(20.0, 20.0, 20.0, 20.0));
        mainPane.getChildren().add(gPane);

        int i = 1;

        for (Produkt p : produkter) {
            TextField tempName = new TextField(p.getNavn());
            TextField tempDesc = new TextField(p.getBeskrivelse());
            TextField tempInkobsPris = new TextField(""+p.getIndkobsPris());
            TextField tempSalgspris = new TextField(""+p.getSalgsPris());

            gPane.addRow(i, tempName, tempDesc, tempInkobsPris, tempSalgspris);
            i++;
        }

        Label totalSolgt = new Label("Antal solgte * pris");
        Label vareNavn = new Label("Vare Navn");
        Label prisPrStk = new Label("Pris pr. stk.");
        prisPrStk.setStyle("-fx-font-weight: 900");
        vareNavn.setStyle("-fx-font-weight: 900");
        totalSolgt.setStyle("-fx-font-weight: 900");
        gPane.addRow(0, new Label(), new Label(), vareNavn, prisPrStk, totalSolgt);
    }
}
