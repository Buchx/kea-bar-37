package sample.Controllers;

import com.sun.rowset.internal.Row;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import sample.Logic.ProduktListe;
import sample.Objects.Produkt;

import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.concurrent.atomic.AtomicInteger;

public class Controller implements Initializable {
    @FXML
    AnchorPane salgPane;
    @FXML
    Button gotoP;
    private ProduktListe l = new ProduktListe();
    private GridPane gPane = new GridPane();
    private ArrayList<Produkt> produkter = l.produktListe;
    private Label sumLabel = new Label();
    Date d = new Date();
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        gotoP.setOnAction(e -> gemDagensSalg());
        l.createFile();
        l.readFromFile();
        updateSum();        //Called to show the label
        createProducts();
        gemDagensSalg();
    }

    private void createProducts() {
        gPane.setVgap(10);
        gPane.setHgap(30);
        gPane.setPadding(new Insets(20.0, 20.0, 20.0, 20.0));
        salgPane.getChildren().add(gPane);

        int i = 1;

        for (Produkt p : produkter) {
            Button tempAdd = new Button(" + ");
            tempAdd.setMinWidth(60);
            tempAdd.setStyle("-fx-background-color: #14ff16");
            tempAdd.setId(p.getNavn() + "+");

            Button tempRemove = new Button(" - ");
            tempRemove.setMinWidth(30);
            tempRemove.setStyle("-fx-background-color: #ff5e4e");
            tempRemove.setId(p.getNavn() + "-");

            Label tempName = new Label(p.getNavn());
            tempName.setId(p.getNavn() + "Name");

            Label tempPris = new Label("" + p.getSalgsPris());

            Label tempAmountSold = new Label("0");
            tempAmountSold.setId(p.getNavn() + "TotalSold");

            AtomicInteger amountSold = new AtomicInteger();

            tempAdd.setOnAction(e -> {
                amountSold.getAndIncrement();
                p.setAntalSolgt(amountSold.get());
                int value = p.getAntalSolgt() * p.getSalgsPris();
                tempAmountSold.setText("" + value + ",-");
                updateSum();
            });
            tempRemove.setOnAction(e -> {
                amountSold.getAndDecrement();
                p.setAntalSolgt(amountSold.get());
                int value = p.getAntalSolgt() * p.getSalgsPris();
                tempAmountSold.setText("" + value + ",-");
                updateSum();
            });

            gPane.addRow(i, tempAdd, tempRemove, tempName, tempPris, tempAmountSold);
            i++;
        }

        Label totalSolgt = new Label("Antal solgte * pris");
        Label vareNavn = new Label("Vare Navn");
        Label prisPrStk = new Label("Pris pr. stk.");
        prisPrStk.setStyle("-fx-font-weight: 900");
        vareNavn.setStyle("-fx-font-weight: 900");
        totalSolgt.setStyle("-fx-font-weight: 900");
        gPane.addRow(0, gotoP, new Label(), vareNavn, prisPrStk, totalSolgt);
        gPane.addRow(produkter.size() + 1, new Label(), new Label(), new Label(), new Label(), sumLabel);
    }

    private void updateSum() {
        int sum = 0;
        for (Produkt p : produkter) {
            sum += p.getSalgsPris() * p.getAntalSolgt();
        }
        sumLabel.setText("" + sum + ",- DKK");
        sumLabel.setStyle("-fx-font-weight: 900");
    }

    private void gemDagensSalg() {
        try {

            PrintStream printStream = new PrintStream(System.getProperty("user.home") + "/KEABAR/salg/"+sdf.format(d) + ".txt");
            printStream.println("Navn;Salgspris;Antalsolgt");
            for (Produkt p : produkter) {
               printStream.println(
                       p.getNavn() + ";"
                       + p.getSalgsPris() + ";"
                       + p.getAntalSolgt()
               );
            }
            printStream.println("Sum for dagen: "+sumLabel.getText());


        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        }
    }
}
