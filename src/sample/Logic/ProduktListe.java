package sample.Logic;

import sample.Objects.Produkt;
import java.io.*;
import java.util.ArrayList;

public class ProduktListe {

    public ArrayList<Produkt> produktListe = new ArrayList<>();

    private final File file = new File(System.getProperty("user.home") + "/KEABAR/produkter.txt");
    private final File salgDir = new File(System.getProperty("user.home") + "/KEABAR/salg/README.txt");

    //OPRET FIL
    public void createFile() {
        try {
            if (!file.canWrite()) {
                file.getParentFile().mkdirs();
                file.createNewFile();
                System.out.println("CREATED FILE: " + file.getAbsolutePath());
                writeToFileOnce();
            }
            if(!salgDir.canWrite()){
                salgDir.getParentFile().mkdirs();
                salgDir.createNewFile();

            }
        } catch (IOException e) {
            e.getMessage();
        }

    }

    //WRITE TO FILE
    public void writeToFileOnce() {
        try (FileWriter fw = new FileWriter(file, true);
             BufferedWriter bw = new BufferedWriter(fw);
             PrintWriter out = new PrintWriter(bw)) {
            out.println("Øl;Smager godt;5;10");
            out.println("Sodavand;Smager godt;3;5");
            out.println("Drinks;Smager godt;10;20");
            out.println("Chips;Smager godt;3;5");
            out.println("Popcorn;Smager godt;3;5");
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    //LÆS FRA FIL
    public void readFromFile() {

        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            for (String line; (line = br.readLine()) != null; ) {
                String[] parts = line.split(";");
                String navn = parts[0];
                String beskrivelse = parts[1];
                int kobsPris = Integer.parseInt(parts[2]);
                int salgsPris = Integer.parseInt(parts[3]);
                produktListe.add(new Produkt(navn, beskrivelse, kobsPris, salgsPris));
            }
            // line is not visible here.
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

    }

    //OPDATER FIL

    //SLET FRA FIL
}
