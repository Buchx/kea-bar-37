package sample.Objects;

public class Produkt {
    private String navn;            //Navnet på varen
    private String beskrivelse;     //En kort beskrivelse af varen
    private int indkobsPris;        //Hvilken pris køber vi den til
    private int salgsPris;          //Hvad sælger vi den til?
    private int fortjeneste;        //Hvad tjener vi på den?
    private int antalSolgt;

    public Produkt(String navn, String beskrivelse, int indkobsPris, int salgsPris) {
        this.navn = navn;
        this.beskrivelse = beskrivelse;
        this.indkobsPris = indkobsPris;
        this.salgsPris = salgsPris;
    }

    public int getAntalSolgt() {
        return antalSolgt;
    }

    public void setAntalSolgt(int antalSolgt) {
        this.antalSolgt = antalSolgt;
    }

    public String getNavn() {
        return navn;
    }

    public void setNavn(String navn) {
        this.navn = navn;
    }

    public String getBeskrivelse() {
        return beskrivelse;
    }

    public void setBeskrivelse(String beskrivelse) {
        this.beskrivelse = beskrivelse;
    }

    public int getIndkobsPris() {
        return indkobsPris;
    }

    public void setIndkobsPris(int indkobsPris) {
        this.indkobsPris = indkobsPris;
    }

    public int getSalgsPris() {
        return salgsPris;
    }

    public void setSalgsPris(int salgsPris) {
        this.salgsPris = salgsPris;
    }

    public int getFortjeneste() {
        return fortjeneste;
    }

    public void setFortjeneste(int fortjeneste) {
        this.fortjeneste = fortjeneste;
    }
}
